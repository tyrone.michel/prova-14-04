#!/bin/bash
echo -e "\nShell padrão:\n"
echo ${SHELL}

echo -e "\ndefine diretórios de procura por programas executados no shell:\n"
echo ${PATH}

echo -e "\nDiretório atual:\n"
echo ${PWD}

echo -e "\nTempo de execução:\n"
echo ${SECONDS}

echo -e "\ninforma o nome do usuário do shell:\n"
echo ${USER}

echo -e "\ninforma o caminho do diretório home do usuário:\n"
echo ${HOME}

echo -e "\nIdioma/Linguagem, especificada como locale:\n"
echo ${LANG}

echo -e "\nTipo de terminal atual em uso:\n"
echo ${TERM}

echo -e "\nUID do usuário atual.:\n"
echo ${UID}

echo -e "\nGera um número aleatório:\n"
echo ${RANDOM}


echo "Fim"
